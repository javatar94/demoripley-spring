package main.java.com.example.avatar.demoRipley;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoRipleyApplication {
	WebDriver driver;

	@Before
	public void setUp(){
		System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver");
		driver = new ChromeDriver();
	}

	@Test
	public void loginTest() throws InterruptedException {
		driver.get("https://simple.ripley.com.pe/huawei-router-wi-fi-ws318n-2004262505264p");
		driver.manage().window().maximize();
		Thread.sleep(20000);
		driver.findElement(By.cssSelector("button#onesignal-slidedown-cancel-button.align-right.secondary.slidedown-button")).click();
		Thread.sleep(7000);
		driver.findElement(By.cssSelector("button#buy-button.btn-loading.btn-primary.btn-loading.js-buy-button.with-withlist")).click();
		//Thread.sleep(7000);
		//driver.findElement(By.cssSelector("div.button > button")).click();
		Thread.sleep(4000);
		driver.findElement(By.id("stepper-button")).click();
		Thread.sleep(7000);
		driver.findElement(By.cssSelector("button#guestShopperContinue.btn.btn-primary")).click();
		Thread.sleep(7000);
		driver.findElement(By.id("WC_CheckoutLogon_FormInput_logonId")).click();
		driver.findElement(By.id("WC_CheckoutLogon_FormInput_logonId")).clear();
		driver.findElement(By.id("WC_CheckoutLogon_FormInput_logonId")).sendKeys("75268234");
		driver.findElement(By.id("WC_CheckoutLogon_FormInput_logonPassword")).clear();
		driver.findElement(By.id("WC_CheckoutLogon_FormInput_logonPassword")).sendKeys("tremendaloka01");
		driver.findElement(By.cssSelector("button.btn.btn-secondary.center")).click();
		Thread.sleep(15000);
		driver.findElement(By.id("singleShipmentPhysicalStore_0_17017")).click();
		Thread.sleep(10000);
		driver.findElement(By.id("WC_ShipmentDisplay_continue")).click();
		Thread.sleep(10000);
		driver.findElement(By.id("WC_ShipmentDisplay_continue_modal")).click();
		Thread.sleep(10000);
		driver.findElement(By.cssSelector("span#payMethodId_1_3_etiqueta_icono.name")).click();
		//Thread.sleep(10000);
		//driver.findElement(By.id("shippingBillingPageNext")).click();
	}

	//@AfterTest
    /*public void teardown(){
        driver.quit();
    }*/

}
